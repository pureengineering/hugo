Pure Engineering was originally started in 2006 to offer specialized consulting and design services for small projects and ideas. Then in 2014 Pure Engineering LLC was established as the growth of the consulting work and products exploded. Our services cover a wide range of skills starting from strategic planning and product development. We offer architecture development, applying engineering rigor to develop and assess technology that is suitable for the design. In most cases you need more than a piece of paper; this is where we can develop a proof of concept to help conceptualize your product. Beyond a concept, we will make a prototype to demonstrate the produce experience and shake out all the issues before moving to manufacturing. In most cases if we are involved early in the design process, we will design for manufacturing from the beginning ensuring you will be prepared for production. In some cases when you have hit a design block or impossible requirements, we will provide subject matter experts to find a solution. Finally as we are applied researchers at heart we love to apply our engineering backgrounds to develop unproven technologies, test out theories and live on the bleeding edge.

IoT
The Internet of Things is our major focus area. We have been working on IoT systems before they were called IoT. Our strength lays with low power systems that run from batteries, as well as the movement of data across low power system to get to a server. We work with a wide range of wireless technologies to support any application. 

Android Application Development
Android is an operating system for mobile devices such as cellular phones, tablet computers and netbooks. Android is developed by Google's developers and programmers and is based upon the Linux kernel and GNU software. Android has a large community of developers writing application programs that extend the functionality of the devices. Developers write managed code in the Java language, controlling the device via Google-developed Java libraries. We have written a number of applications and are widely used by end users in the field. Some applications were downloaded over 100k on the play store. In addition to Application development we have experience with the ASOP source code and have made custom modifications to it. We have developed custom ROM’s and tools from the ASOP source code. 

Software Defined Radio Hardware and Firmware
We have extensive knowledge on designing hardware for Software Defined Radio (SDR) systems as well as writing the embedded software to make them run. We are aware of noise, cross-talk, signal loss, and impedance matching that are critical in making a working system.    

BLE (Bluetooth Low Energy) sensor development
Bluetooth Low energy is an explosive new technology in the marketplace today. We have extensive experience making sensors that connect to modern cellular phones and computers. We have proven designs that run from several uWatts of power enabling your device to run for years without a battery change.  We are familiar with the entire BLE stack and can make custom protocols using COTS hardware. 
We have experience with the following BLE micro-controllers
Nordic nrf5x series
TI cc26xx series
Cypress PSoc 4 BLE
STM BlueNRG
Dialog SmartBond
and Others
Thermal Sensor Integration
We have extensive experience with FLIR Lepton Thermal Camera. We were the first to bring to market a simple to use breakout board that enabled video capture from microcontroller development boards. Several thousand systems have been shipped to date. 

Spectrometer Sensor Integration
The Hamamatsu C12666MA Spectrometer is a game changer in terms of size and functionality for a spectrometer. We are supporting this sensor with breakout boards and sample firmware and software to enable the DIY crowd to make sensors to improve the world and how we see it.   

Radiation Sensors
We are intimately familiar Radiation Sensors. We have designed fully custom solutions for the electronics for several detector materials. 

Ground Penetrating Radar
With a number of published articles on GPR, our staff has contributed to the GPR community and familiar with building, testing, and evaluating GPR systems. In addition to GPR, this research has carried over to sonar image reconstruction as well. A number of signal processing techniques have been developed as well that can do target identification and classification based on the signature response of and unknown target. These signal processing techniques can be applied to a number of different applications. see paper

Development Backend
In order to support a wide range of solutions, we have had to understand the tools to make such systems. We are focused on supporting the GCC tool chain for all embedded development. We have experience with a number of different microcontroller families ranging from the AVR, AVR32, ARM M0, M3, M4, A8, PSOC5 and A9. Our backend support also includes revision control management, selection, setup and maintenance. We are familiar with a number of revision management tools such as svn and git.  
Key Skills
Embedded Software development on ARM, Cortex M0+, Cortex M3/4, AVR 32, AVR, Xmega, PIC, and MSP430 using C and assembly. 
Advanced algorithm development, DSP processing, Matlab Simulations
GCC toolchain setup for windows and linux. JTAG + Debugging using gdb. 
High Power (10A+) Switching regulator design. 
High density PCB layout and routing of 0201 and BGA components. 
High Speed FPGA PCB schematics/placement and routing differential and matched pairs. 
PCB Design and Layout, Soldering, Firmware, CPLD, and Software Design 
Extremely low noise trans-impedance amplifier design. 
High voltage power supplies greater than 1000V.
Ultra low power wireless systems running from harvested energy only
Low memory systems running under 4k of ram with OS and application. 
Power optimized hardware/fimware drivers. 
802.11 wifi driver and low level calls to chipset.
Custom bluetooth BLE stack.
Extensive use of a RTOS using preemptive scheduling and message queues. 
Embedded software drivers for: GPS, Accelerometers, Magnetic, Pressure, Humidity, Gyroscopes, Wireless transceivers, Bluetooth, BLE, Serial Flash Memory, Real time Clocks, AD converters, DAC converters, UART, CAN, I2C, SPI, SDIO, USB, Timers, Serial Sensors, CPLD devices, Memory Mapped IO, DMA, and inter-processor communications.
Android application development. Custom UI and Bluetooth interfacing to sensors.
Android ASOP development. ROM + Tool-chain modifications
Setup and maintenance of software revision management system and project management web applications
Example Services
PCB design, schematic design, schematic review, layout, layout review
Processor/micro-controller selection and system architecture 
Firmware development (embedded software development) 
Hardware/software integration
RTOS selection or evaluate the need for an operating system porting 
BSP/bootloaders/linux/rtos to new hardware 
Application development for microcontroller and DSP devices 
USB device development (high speed and USB 3.0)
Wireless sensor development( BLE, Wi-Fi, zigbee, bluetooth, ISM custom) 
Development tool selection and setup 
Revision control management; selection, setup and maintenance
Android application development
Embedded android porting and modifications 
Jobs
Pure Engineering is also Hiring, Please apply here if you would like to join the team: angel.co/pure-engineering/jobs/ or send us your resume. 
